package io.luismiguelss.fightem;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import io.luismiguelss.fightem.TemplateActivities.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Animate FadeInOut
        TextView developedByText = findViewById(R.id.developedByText);

        developedByText.animate().alpha(1).setStartDelay(600).setDuration(1400).withEndAction(
                () -> developedByText.animate().alpha(0).setStartDelay(2000).setDuration(2000)
        );

        // Wait and redirect to game
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }, 6400);

    }
}
