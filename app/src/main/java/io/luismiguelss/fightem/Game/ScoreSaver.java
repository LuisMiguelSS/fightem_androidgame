package io.luismiguelss.fightem.Game;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

public class ScoreSaver {

    // Attribute
    private AppCompatActivity caller;

    // Constructor
    public ScoreSaver(AppCompatActivity caller) {
        this.caller = caller;
    }

    // Getter
    public int getHighestScore() {
        SharedPreferences prefs = caller.getSharedPreferences("score", Context.MODE_PRIVATE);
        return prefs.getInt("highest",0);
    }

    // Setter
    public void trySetNewScore(int score) {
        SharedPreferences prefs = caller.getSharedPreferences("score", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if (prefs.contains("highest")) {

            // Check if new score is higher
            if (prefs.getInt("highest",0) < score) {
                editor.putInt("highest", score);
                editor.commit();
            }
        } else {
            editor.putInt("highest", score);
            editor.commit();
        }
    }

}
