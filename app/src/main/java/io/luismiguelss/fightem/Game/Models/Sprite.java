package io.luismiguelss.fightem.Game.Models;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import androidx.annotation.NonNull;

import java.util.Random;

import io.luismiguelss.fightem.Game.GameView;

public class Sprite {

    // Attributes
    // direction = 0 up, 1 left, 2 down, 3 right,
    // animation = 3 back, 1 left, 0 front, 2 right
    int[] DIRECTION_TO_ANIMATION_MAP = {3, 1, 0, 2};
    protected static final int BMP_COLUMNS = 3;
    protected static final int BMP_ROWS = 4;

    private int x = 0;
    private int y = 0;

    private int width;
    private int height;

    private int xSpeed;
    private int ySpeed;

    private GameView gameView;
    private Bitmap bmp;
    private int currentFrame = 0;
    private boolean alreadyHitPlayer = false;

    //
    // Constructor
    //
    public Sprite(@NonNull GameView gameView, @NonNull Bitmap bmp) {
        this.width = bmp.getWidth() / BMP_COLUMNS;
        this.height = bmp.getHeight() / BMP_ROWS;
        this.gameView = gameView;
        this.bmp = bmp;

        Random rnd = new Random(System.currentTimeMillis());
        xSpeed = rnd.nextInt(20);
        ySpeed = rnd.nextInt(20);
    }

    //
    // Getters
    //
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public GameView getGameView() {
        return gameView;
    }

    public int getCurrentFrame() {
        return currentFrame;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getxSpeed() {
        return xSpeed;
    }

    public int getySpeed() {
        return ySpeed;
    }

    public Bitmap getBmp() {
        return bmp;
    }

    public boolean isAlreadyHitPlayer() {
        return alreadyHitPlayer;
    }

    //
    // Setters
    //
    public void setCurrentFrame(int currentFrame) {
        this.currentFrame = currentFrame;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setxSpeed(int xSpeed) {
        this.xSpeed = xSpeed;
    }

    public void setySpeed(int ySpeed) {
        this.ySpeed = ySpeed;
    }

    public void setAlreadyHitPlayer(boolean alreadyHitPlayer) {
        this.alreadyHitPlayer = alreadyHitPlayer;
    }

    //
    // Methods
    //
    protected void update() {
        if (x >= gameView.getWidth() - width - xSpeed || x + xSpeed <= 0)
            xSpeed = -xSpeed;


        if (y >= gameView.getHeight() - height - ySpeed || y + ySpeed <= 0)
            ySpeed = -ySpeed;

        x = x + xSpeed;
        y = y + ySpeed;

        currentFrame = ++currentFrame % BMP_COLUMNS;
    }

    public void onDraw(Canvas canvas) {
        update();
        int srcX = currentFrame * width;
        int srcY = getAnimationRow() * height;
        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        Rect dst = new Rect(x, y, x + width, y + height);
        canvas.drawBitmap(bmp, src, dst, null);
    }

    // direction = 0 up, 1 left, 2 down, 3 right,
    // animation = 3 back, 1 left, 0 front, 2 right
    protected int getAnimationRow() {
        double dirDouble = (Math.atan2(xSpeed, ySpeed) / (Math.PI / 2) + 2);
        int direction = (int) Math.round(dirDouble) % BMP_ROWS;
        return DIRECTION_TO_ANIMATION_MAP[direction];
    }
}
