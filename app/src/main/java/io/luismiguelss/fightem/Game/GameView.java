package io.luismiguelss.fightem.Game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;

import io.luismiguelss.fightem.Game.Models.Player;
import io.luismiguelss.fightem.Game.Models.Spell;
import io.luismiguelss.fightem.Game.Models.Sprite;
import io.luismiguelss.fightem.R;

public class GameView extends SurfaceView {

    //
    // Attributes
    //
    public static int level = 1;

    private SurfaceHolder holder;
    private GameLoopThread gameLoopThread;

    private Player player;
    private int lives = 3;
    private int default_number_enemies = 5;
    private int score;
    private boolean win = false;

    private ArrayList<Sprite> sprites = new ArrayList<>();
    private ArrayList<Spell> spells = new ArrayList<>();

    //
    // Constructor
    //
    public GameView(Context context) {
        super(context);
        init();
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    //
    // Getter
    //
    public Player getPlayer() {
        return player;
    }
    public int getScore() {
        return score;
    }
    public boolean isWin() {return win;}
    public boolean playerDied() {
        return lives <= 0;
    }
    public int getLives() {
        return lives;
    }

    //
    // Methods
    //
    private void init() {
        setBackgroundColor(Color.TRANSPARENT);
        gameLoopThread = new GameLoopThread(this);
        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                gameLoopThread.setRunning(false);
                while (retry) {
                    try {
                        gameLoopThread.join();
                        retry = false;
                    } catch (InterruptedException e) {
                    }
                }
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                createSprites();
                gameLoopThread.setRunning(true);
                gameLoopThread.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

        });
    }

    private void createSprites() {
        player = createPlayer(R.drawable.player);

        for (int i = 0; i < default_number_enemies*(0.75*level); i++)
            sprites.add(createSprite(R.drawable.enemy));
    }

    private Sprite createSprite(int resource) {
        return new Sprite(this, BitmapFactory.decodeResource(getResources(), resource));
    }

    private Player createPlayer(int resource) {
        return new Player(this, BitmapFactory.decodeResource(getResources(), resource));
    }

    private Paint getBackgroundGradient() {
        Shader shader = new RadialGradient(getWidth() / 2,
                getHeight() / 2,
                (int) (getWidth() / 1.5),
                getContext().getColor(R.color.bgPurple),
                getContext().getColor(R.color.darkModern),
                Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);

        return paint;
    }

    public void shoot() {
        Bitmap fireballBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rsz_fireball_1);
        Spell spell = new Spell(this, fireballBitmap.copy(Bitmap.Config.ARGB_8888, true), player.getX(), player.getY());
        spell.setDirection(player.getOrientation());

        spells.add(spell);

        // Play sound
        new Thread(() -> MediaPlayer.create(getContext(), R.raw.spellthrow).start()).start();
    }

    //
    // Listeners/events
    //
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth(), getBackgroundGradient());

        for (Sprite s : sprites)
            s.onDraw(canvas);

        for (Spell f : spells)
            f.onDraw(canvas);

        player.onDraw(canvas);

        // Check if there are no enemies (Win)
        if (sprites.size() <= 0)
            win = true;

        for (Sprite enemy:sprites) {
            // Check collision with player
            if (player.intersects(enemy.getX(), enemy.getY(), enemy.getBmp().getWidth() / 3, enemy.getHeight() / 3)) {
                if (!enemy.isAlreadyHitPlayer()) {
                    enemy.setAlreadyHitPlayer(true);
                    --lives;
                }
            } else
                enemy.setAlreadyHitPlayer(false);
        }

        // Check to remove spells and/or enemies if they collided.
        synchronized (getHolder()) {

            ArrayList<Spell> spellsToDelete = new ArrayList<>();
            ArrayList<Sprite> enemiesToDelete = new ArrayList<>();

            // Iterate over existing spells and enemies
            for (Spell spell : spells) {
                for (Sprite enemy : sprites) {

                    // Check collision with spell
                    if (spell.intersects(enemy.getX(), enemy.getY(), enemy.getBmp().getWidth() / 3, enemy.getHeight() / 3)) {
                        enemiesToDelete.add(enemy);
                        spellsToDelete.add(spell);
                        score++;
                        break;
                    }
                }

                // Delete enemies
                for (Sprite enemy : enemiesToDelete)
                    sprites.remove(enemy);
                enemiesToDelete.clear();

                if (spell.isOutsideScreen())
                    spellsToDelete.add(spell);
            }

            // Delete spells
            for (Spell spell : spellsToDelete) {
                spells.remove(spell);

                // Play sound
                new Thread(() -> MediaPlayer.create(getContext(), R.raw.spellexplosionpulse).start()).start();
            }
            spellsToDelete.clear();
        }

    }

}
