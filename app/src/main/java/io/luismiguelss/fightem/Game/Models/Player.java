package io.luismiguelss.fightem.Game.Models;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import io.luismiguelss.fightem.Game.GameView;

public class Player extends Sprite {

    //
    // Attributes
    //
    private static int steps = 15;
    private boolean isMoving;
    private Orientation orientation = Orientation.BOTTOM;

    public enum Orientation {
        TOP(0, -steps),
        TOP_RIGHT(steps, -steps),
        RIGHT(steps, 0),
        BOTTOM_RIGHT(steps, steps),
        BOTTOM(0, steps),
        BOTTOM_LEFT(-steps, steps),
        LEFT(-steps, 0),
        TOP_LEFT(-steps, -steps);

        int x;
        int y;

        Orientation(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    //
    // Constructor
    //
    public Player(GameView gameView, Bitmap bmp) {
        super(gameView, bmp);

        isMoving = false;

        setxSpeed(3);
        setySpeed(3);

        setX(gameView.getWidth() / 2 - getWidth() / 2);
        setY(gameView.getHeight() / 2 - getHeight() / 2);
    }

    //
    // Getter
    //
    public Orientation getOrientation() {
        return orientation;
    }

    //
    // Setter
    //
    public void setMoving(boolean moving) {
        isMoving = moving;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    //
    // Methods
    //
    public Orientation getMovableOrientation(Orientation orientation) {
        switch (orientation) {
            case TOP:
                if (getY() >= 5)
                    return orientation;
                return null;

            case LEFT:
                if (getX() >= 5)
                    return orientation;
                return null;

            case RIGHT:
                if (getX() + getWidth() <= getGameView().getWidth() - 5)
                    return orientation;
                return null;

            case BOTTOM:
                if (getY() + getHeight() <= getGameView().getHeight() - 5)
                    return orientation;
                return null;

            case TOP_LEFT:
                if (getY() >= 5)
                    if (getX() >= 5)
                        return orientation;
                    else
                        return Orientation.TOP;
                else if (getX() >= 5)
                    return Orientation.LEFT;
                else
                    return null;

            case TOP_RIGHT:
                if (getY() >= 5)
                    if (getX() + getHeight() <= getGameView().getWidth() - 5)
                        return orientation;
                    else
                        return Orientation.TOP;
                else if (getX() + getWidth() <= getGameView().getWidth() - 5)
                    return Orientation.RIGHT;
                else
                    return null;

            case BOTTOM_LEFT:
                if (getY() + getHeight() <= getGameView().getHeight() - 5)
                    if (getX() >= 5)
                        return orientation;
                    else
                        return Orientation.BOTTOM;
                else if (getX() >= 5)
                    return Orientation.LEFT;
                else
                    return null;

            case BOTTOM_RIGHT:
                if (getY() + getHeight() <= getGameView().getHeight() - 5)
                    if (getX() + getWidth() <= getGameView().getWidth() - 5)
                        return orientation;
                    else
                        return Orientation.BOTTOM;
                else if (getX() + getWidth() <= getGameView().getWidth() - 5)
                    return Orientation.RIGHT;
                else
                    return null;

            default:
                return null;
        }
    }

    public boolean intersects(int x, int y, int width, int height) {
        Rect thisSprite = new Rect(getX(), getY(), getX() + getWidth(), getY() + getHeight());
        Rect otherSprite = new Rect(x, y, x + width, y + height);
        return thisSprite.intersect(otherSprite);
    }

    @Override
    protected void update() {

        if (orientation != null && getMovableOrientation(orientation) != null) {
            orientation = getMovableOrientation(orientation);

            setX(getX() + orientation.x);
            setY(getY() + orientation.y);

            setCurrentFrame( (getCurrentFrame() + 1) % BMP_COLUMNS);
        }
    }

    @Override
    protected int getAnimationRow() {
        double dirDouble = (Math.atan2(orientation.x, orientation.y) / (Math.PI / 2) + 2);
        int direction = (int) Math.round(dirDouble) % BMP_ROWS;
        return DIRECTION_TO_ANIMATION_MAP[direction];
    }

    //
    // Events
    //
    @Override
    public void onDraw(Canvas canvas) {
        int spriteX = getWidth();
        int spriteY = 0;

        Rect source = new Rect(spriteX, spriteY, spriteX + getWidth(), spriteY + getHeight());
        Rect destination = new Rect(getX(), getY(), getX() + getWidth(), getY() + getHeight());

        if (isMoving) {
            update();
            spriteX = getCurrentFrame() * getWidth();
            spriteY = getAnimationRow() * getHeight();

            source = new Rect(spriteX, spriteY, spriteX + getWidth(), spriteY + getHeight());

        }

        canvas.drawBitmap(getBmp(), source, destination, null);

    }
}
