package io.luismiguelss.fightem.Game.Models;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

import io.luismiguelss.fightem.Game.GameView;

public class Spell {

    //
    // Attributes
    //
    private GameView gameView;
    private Bitmap bmp;
    private int x;
    private int y;
    private Player.Orientation direction = Player.Orientation.RIGHT;

    //
    // Constructor
    //
    public Spell(GameView gameView, Bitmap bmp, int x, int y) {
        this.gameView = gameView;
        this.bmp = bmp;
        this.x = x;
        this.y = y;
    }

    //
    // Getter
    //
    public boolean isOutsideScreen() {
        return (x+bmp.getWidth() < 0 || x > gameView.getRight() ||
            y+bmp.getHeight() < 0 || y > gameView.getBottom());
    }

    //
    // Setters
    //
    public void setDirection(Player.Orientation direction) {
        this.direction = direction;
        bmp = rotateBitmap(bmp,getAngleByOrientation());
    }

    //
    // Methods
    //
    private int getAngleByOrientation() {
        if (direction != null) {
            switch (direction) {
                case TOP:
                    return 270;
                case LEFT:
                    return 180;
                case BOTTOM:
                    return 90;
                case TOP_LEFT:
                    return 225;
                case TOP_RIGHT:
                    return -45;
                case BOTTOM_LEFT:
                    return 135;
                case BOTTOM_RIGHT:
                    return 45;
                case RIGHT:
                    return 0;
                default: return 0;
            }
        }
        return 0;
    }
    public void onDraw(Canvas canvas) {
        if (direction != null) {
            x += direction.x*2;
            y += direction.y*2;
        }

        // Paint bitmap
        canvas.drawBitmap(bmp, x, y, null);
    }

    public boolean intersects(int x, int y, int width, int height) {
        Rect thisSprite = new Rect(this.x, this.y, this.x + bmp.getWidth(), this.y + bmp.getHeight());
        Rect otherSprite = new Rect(x, y, x + width, y + height);

        return thisSprite.intersect(otherSprite);
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}
