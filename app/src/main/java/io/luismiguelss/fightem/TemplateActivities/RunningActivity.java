package io.luismiguelss.fightem.TemplateActivities;

import android.content.Intent;
import android.media.AudioManager;
import android.view.KeyEvent;

import io.luismiguelss.fightem.Services.MusicService;


public class RunningActivity extends BaseActivity {

    //
    // Methods
    //
    protected void setVolume(double percentage) {

        if (audioManager != null) {
            int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            double calculated = percentage / 100;
            double result = maxVolume * calculated;
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) result, AudioManager.ADJUST_SAME);
        }

    }

    protected boolean isMuted() {
        return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0;
    }

    //
    // Listeners
    //
    @Override
    public void onPause() {
        super.onPause();
        stopService(new Intent(this, MusicService.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(new Intent(this, MusicService.class));
    }

    // Volume keys
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
                break;
            case KeyEvent.KEYCODE_VOLUME_UP:
                audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
                break;
        }

        return true;
    }

}
