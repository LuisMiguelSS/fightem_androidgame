package io.luismiguelss.fightem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.TextView;

import io.luismiguelss.fightem.Game.GameView;
import io.luismiguelss.fightem.Game.ScoreSaver;

public class LoseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lose);

        MediaPlayer.create(this,R.raw.lose).start();

        GameView.level = 1;

        // Home
        findViewById(R.id.btnBackLose).setOnClickListener(v -> {
            startActivity(new Intent(LoseActivity.this, MainActivity.class));
            finish();
        });

        ((TextView)findViewById(R.id.textHighestScore)).setText("Puntuación más alta: " + new ScoreSaver(this).getHighestScore());
    }
}
