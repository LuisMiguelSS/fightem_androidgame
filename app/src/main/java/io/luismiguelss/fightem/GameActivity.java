package io.luismiguelss.fightem;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import io.github.controlwear.virtual.joystick.android.JoystickView;
import io.luismiguelss.fightem.Game.GameView;
import io.luismiguelss.fightem.Game.Models.Player;
import io.luismiguelss.fightem.Game.ScoreSaver;
import io.luismiguelss.fightem.TemplateActivities.BaseActivity;

public class GameActivity extends BaseActivity {

    //
    // Attributes
    //
    final int refreshRate = 17; // 60 per second
    private GameView surface;
    private JoystickView joystick;
    private ImageButton attackBtn;
    private TextView labelDeaths;
    private long lastClick;

    //
    // Methods
    //
    private void movePlayer(int angle) {
        Player.Orientation orientation;

        if (angle >= 15 && angle < 75)
            orientation = Player.Orientation.TOP_RIGHT;
        else if (angle >= 75 && angle < 105)
            orientation = Player.Orientation.TOP;
        else if (angle >= 105 && angle < 165)
            orientation = Player.Orientation.TOP_LEFT;
        else if (angle >= 165 && angle < 195)
            orientation = Player.Orientation.LEFT;
        else if (angle >= 195 && angle < 255)
            orientation = Player.Orientation.BOTTOM_LEFT;
        else if (angle >= 255 && angle < 285)
            orientation = Player.Orientation.BOTTOM;
        else if (angle >= 285 && angle < 345)
            orientation = Player.Orientation.BOTTOM_RIGHT;
        else
            orientation = Player.Orientation.RIGHT;

        surface.getPlayer().setOrientation(orientation);
    }
    private void goToWin() {
        new ScoreSaver(this).trySetNewScore(surface.getScore());
        startActivity(new Intent(GameActivity.this, WinActivity.class));
        finish();
    }
    private void goToLose() {
        new ScoreSaver(this).trySetNewScore(surface.getScore());
        startActivity(new Intent(GameActivity.this, LoseActivity.class));
        finish();
    }

    //
    // Events
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        setContentView(R.layout.activity_game);

        // Surface
        surface = findViewById(R.id.gameView);

        // Move character by Joystick
        joystick = findViewById(R.id.joystick);
        joystick.setOnMoveListener((angle, strength) -> {

            // Check if press was released
            if (angle == 0 && strength == 0)
                surface.getPlayer().setMoving(false);
            else {
                surface.getPlayer().setMoving(true);
                movePlayer(angle);
            }
        }, refreshRate);


        // Attack button
        attackBtn = findViewById(R.id.buttonSword);
        attackBtn.setOnClickListener(v -> {
            if (System.currentTimeMillis() - lastClick > 500) {
                lastClick = System.currentTimeMillis();
                surface.shoot();
            }
        });

        // Score count
        labelDeaths = findViewById(R.id.textViewDeaths);
        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(800);
                        runOnUiThread(() -> {
                            labelDeaths.setText("Muertes: " + surface.getScore());

                            // Remove hearts
                            if (surface.getLives() == 2)
                                findViewById(R.id.live3).setVisibility(View.INVISIBLE);
                            else if (surface.getLives() == 1) {
                                findViewById(R.id.live3).setVisibility(View.INVISIBLE);
                                findViewById(R.id.live2).setVisibility(View.INVISIBLE);
                            }
                            else if (surface.getLives() <= 0) {
                                findViewById(R.id.live3).setVisibility(View.INVISIBLE);
                                findViewById(R.id.live2).setVisibility(View.INVISIBLE);
                                findViewById(R.id.live1).setVisibility(View.INVISIBLE);
                            }
                            else {
                                findViewById(R.id.live1).setVisibility(View.VISIBLE);
                                findViewById(R.id.live2).setVisibility(View.VISIBLE);
                                findViewById(R.id.live3).setVisibility(View.VISIBLE);
                            }

                            // Check if player won or lost
                            if(surface.isWin()) {
                                goToWin();
                                interrupt();
                            }
                            else if (surface.playerDied()) {
                                goToLose();
                                interrupt();
                            }
                        });
                    }
                } catch (InterruptedException e) { }
            }
        };

        thread.start();


    }
}
