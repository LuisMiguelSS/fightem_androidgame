package io.luismiguelss.fightem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import io.luismiguelss.fightem.Game.GameView;
import io.luismiguelss.fightem.Game.ScoreSaver;
import io.luismiguelss.fightem.R;

public class WinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);

        MediaPlayer.create(this,R.raw.win).start();

        // Home
        findViewById(R.id.btnBackWin).setOnClickListener(v -> {
            GameView.level = 1;
            startActivity(new Intent(WinActivity.this, MainActivity.class));
            finish();
        });

        // Next level
        findViewById(R.id.btnNextLevel).setOnClickListener(v -> {
            GameView.level += 1;
            startActivity(new Intent(WinActivity.this, GameActivity.class));
            finish();
        });

        ((TextView)findViewById(R.id.textHighestScore)).setText("Puntuación más alta: " + new ScoreSaver(this).getHighestScore());

    }
}
