package io.luismiguelss.fightem;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import io.luismiguelss.fightem.Services.MusicService;
import io.luismiguelss.fightem.TemplateActivities.RunningActivity;

public class MainActivity extends RunningActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        overridePendingTransition(R.anim.fade_in_fast, R.anim.fade_out);

        // Filter image
        ((ImageView) findViewById(R.id.bgSplashImage)).setColorFilter(Color.argb(60, 13, 8, 7));

        findViewById(R.id.playBtn).setOnClickListener(v -> {
            if (!isMuted())
                setVolume(50);
            MediaPlayer player = MediaPlayer.create(this, R.raw.witchlaugh);
            player.start();

            startActivity(new Intent(MainActivity.this, GameActivity.class));

            new Handler().postDelayed(() -> {
                player.stop();
                player.release();

            }, 3000);
        });

        // Play music
        startService(new Intent(this, MusicService.class));
    }
}
